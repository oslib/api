package jwt

import (
	"crypto/ed25519"
	"fmt"
	"golang.org/x/crypto/ssh"
	"io/ioutil"
)

// GetPrivateKey parse pem file with ed25519 private key
func GetPrivateKey(location string) (ed25519.PrivateKey, error) {
	pem, err := ioutil.ReadFile(location)
	if err != nil {
		return nil, err
	}
	privateKey, err := ssh.ParseRawPrivateKey(pem)
	if err != nil {
		return nil, err
	}
	ed25519Key, ok := privateKey.(*ed25519.PrivateKey)
	if !ok {
		return nil, fmt.Errorf("key is not ed25519 private key")
	}
	return *ed25519Key, nil
}

// GetPublicKey parse pem file with ed25519 public key
func GetPublicKey(location string) (ed25519.PublicKey, error) {
	pem, err := ioutil.ReadFile(location)
	if err != nil {
		return nil, err
	}

	// Parse the key
	publicKey, _, _, _, err := ssh.ParseAuthorizedKey(pem)
	if err != nil {
		return nil, err
	}

	// Get key bytes by marshalling and unmarshalling wire format
	var w struct {
		Name     string
		KeyBytes []byte
	}
	if err = ssh.Unmarshal(publicKey.Marshal(), &w); err != nil {
		return nil, err
	}

	// Verify key length
	if len(w.KeyBytes) != ed25519.PublicKeySize {
		return nil, fmt.Errorf("key is not ed25519 public key")
	}
	return w.KeyBytes, nil
}

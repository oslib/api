package jwt

import (
	"crypto/ed25519"
	"fmt"
	"github.com/golang-jwt/jwt/v4"
	"log"
	"sync"
	"time"
)

type Signer struct {
	private ed25519.PrivateKey
	public  ed25519.PublicKey
}

var signer *Signer
var once sync.Once

func GetSigner(privateKeyFile, publicKeyFile string) *Signer {
	once.Do(func() {
		private, err := GetPrivateKey(privateKeyFile)
		if err != nil {
			log.Panicf("unable to parse private key %s", err)
		}
		public, err := GetPublicKey(publicKeyFile)
		if err != nil {
			log.Panicf("unable to parse public key %s", err)
		}
		signer = &Signer{
			private: private,
			public:  public,
		}
	})
	return signer
}

// Sign creates a JWT with the UUID / expiration date encoded in it
func (s *Signer) Sign(uuid string, expiresAt *time.Time) (string, error) {
	var expiration *jwt.NumericDate
	if expiresAt != nil {
		expiration = jwt.NewNumericDate(*expiresAt)
	}
	token := jwt.NewWithClaims(jwt.SigningMethodEdDSA, jwt.RegisteredClaims{
		ExpiresAt: expiration,
		ID:        uuid,
	})
	return token.SignedString(s.private)
}

// Verify validates a signed token with the public key
func (s *Signer) Verify(token string) (*jwt.RegisteredClaims, error) {
	parsed, err := jwt.ParseWithClaims(token, &jwt.RegisteredClaims{}, func(token *jwt.Token) (interface{}, error) {
		return s.public, nil
	})
	if err != nil {
		return nil, err
	}
	if !parsed.Valid {
		return nil, fmt.Errorf("invalid token: %v", err)
	}
	claims, ok := parsed.Claims.(*jwt.RegisteredClaims)
	if !ok {
		return nil, fmt.Errorf("invalid claims format")
	}
	return claims, nil
}

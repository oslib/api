package books

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"time"
)

var (
	NotFound = errors.New("the book could not be found in any API")
)

type Book struct {
	Title       string
	Description string
	ISBN        string
	Image       string
	Publisher   string
	PageCount   int64
	PublishedAt int64
	Authors     []string
	Genres      []string
}

type API interface {
	GetBook(isbn string) (*Book, error)
}

var apis = []API{
	new(GoogleBooksAPI),
}

func GetBook(isbn string) (*Book, error) {
	for _, api := range apis {
		if book, err := api.GetBook(isbn); err == nil {
			return book, nil
		}
	}
	return nil, NotFound
}

func executeRequest(url string, response interface{}) error {
	// Prepare request
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return err
	}
	req.Header.Set("Accept", "application/json")

	// Execute request
	httpClient := &http.Client{Timeout: 10 * time.Second}
	res, err := httpClient.Do(req)
	if err != nil {
		return err
	}
	defer func() {
		if err = res.Body.Close(); err != nil {
			log.Printf("Error closing body: %v", err)
		}
	}()

	if res.StatusCode < http.StatusOK || res.StatusCode >= http.StatusBadRequest {
		return fmt.Errorf("received status code %d", res.StatusCode)
	}

	// Parse response
	return json.NewDecoder(res.Body).Decode(response)
}

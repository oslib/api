package books

import (
	"errors"
	"fmt"
	"github.com/golang-module/carbon"
	"log"
)

type GoogleBooksAPI struct{}

type Response struct {
	Kind       string `json:"kind"`
	TotalItems int64  `json:"totalItems"`
	Items      []struct {
		Kind       string `json:"kind"`
		Id         string `json:"id"`
		Etag       string `json:"etag"`
		SelfLink   string `json:"selfLink"`
		VolumeInfo struct {
			Title               string   `json:"title"`
			Authors             []string `json:"authors"`
			Publisher           string   `json:"publisher"`
			PublishedDate       string   `json:"publishedDate"`
			Description         string   `json:"description"`
			IndustryIdentifiers []struct {
				Type       string `json:"type"`
				Identifier string `json:"identifier"`
			} `json:"industryIdentifiers"`
			ReadingModes struct {
				Text  bool `json:"text"`
				Image bool `json:"image"`
			} `json:"readingModes"`
			PageCount        int64    `json:"pageCount"`
			PrintType        string   `json:"printType"`
			MainCategory     string   `json:"mainCategory"`
			Categories       []string `json:"categories"`
			MaturityRating   string   `json:"maturityRating"`
			AllowAnonLogging bool     `json:"allowAnonLogging"`
			ContentVersion   string   `json:"contentVersion"`
			ImageLinks       struct {
				SmallThumbnail string `json:"smallThumbnail"`
				Thumbnail      string `json:"thumbnail"`
				Small          string `json:"small"`
				Medium         string `json:"medium"`
				Large          string `json:"large"`
				ExtraLarge     string `json:"extraLarge"`
			} `json:"imageLinks"`
			PanelizationSummary struct {
				ContainsEpubBubbles  bool `json:"containsEpubBubbles"`
				ContainsImageBubbles bool `json:"containsImageBubbles"`
			} `json:"panelizationSummary"`
			Language            string `json:"language"`
			PreviewLink         string `json:"previewLink"`
			InfoLink            string `json:"infoLink"`
			CanonicalVolumeLink string `json:"canonicalVolumeLink"`
		} `json:"volumeInfo"`
		SaleInfo struct {
			Country     string `json:"country"`
			Saleability string `json:"saleability"`
			IsEbook     bool   `json:"isEbook"`
		} `json:"saleInfo"`
		AccessInfo struct {
			Country                string `json:"country"`
			Viewability            string `json:"viewability"`
			Embeddable             bool   `json:"embeddable"`
			PublicDomain           bool   `json:"publicDomain"`
			TextToSpeechPermission string `json:"textToSpeechPermission"`
			Epub                   struct {
				IsAvailable bool `json:"isAvailable"`
			} `json:"epub"`
			Pdf struct {
				IsAvailable bool `json:"isAvailable"`
			} `json:"pdf"`
			WebReaderLink       string `json:"webReaderLink"`
			AccessViewStatus    string `json:"accessViewStatus"`
			QuoteSharingAllowed bool   `json:"quoteSharingAllowed"`
		} `json:"accessInfo"`
		SearchInfo struct {
			TextSnippet string `json:"textSnippet"`
		} `json:"searchInfo"`
	} `json:"items"`
}

func (a *GoogleBooksAPI) GetBook(isbn string) (*Book, error) {
	url := fmt.Sprintf("https://www.googleapis.com/books/v1/volumes?q=isbn:%s&maxResults=1", isbn)
	var response Response
	if err := executeRequest(url, &response); err != nil {
		log.Printf("ERROR: %v", err)
		return nil, err
	}
	if len(response.Items) == 0 {
		return nil, errors.New("could not find book in Google Books API")
	}

	book := response.Items[0].VolumeInfo

	// Find the biggest image
	var imageLink string
	if el := book.ImageLinks.ExtraLarge; el != "" {
		imageLink = el
	} else if l := book.ImageLinks.Large; l != "" {
		imageLink = l
	} else if m := book.ImageLinks.Medium; m != "" {
		imageLink = m
	} else if s := book.ImageLinks.Small; s != "" {
		imageLink = s
	} else if t := book.ImageLinks.Thumbnail; t != "" {
		imageLink = t
	} else if st := book.ImageLinks.SmallThumbnail; st != "" {
		imageLink = st
	}

	return &Book{
		Title:       book.Title,
		Description: book.Description,
		Image:       imageLink,
		ISBN:        isbn,
		Publisher:   book.Publisher,
		PageCount:   book.PageCount,
		PublishedAt: int64(carbon.Parse(book.PublishedDate).Year()),
		Authors:     book.Authors,
		Genres:      book.Categories,
	}, nil
}

package slice

func Contains[T comparable](slice []T, el T) bool {
	for _, e := range slice {
		if e == el {
			return true
		}
	}
	return false
}

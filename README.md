# Library API
An open sources library API in which you can search for books, save books to you own account and more.

## Key pair
Generate a key pair (for signing authentication tokens) with `ssh-keygen -m pkcs8 -t ed25519`.

## Environment file
Setup an environment file with the following variables:
```bash
DB_NAME="library" # Can be anything
DB_USER="api" # Can be anything
DB_PASSWORD="<password>"
DB_PORT="4000" # Can be anything
DB_FILES="./_dbfiles" # Advisable to let the name start with an _ so Go ignores the folder
DB_MIGRATE_DSN="postgres://<DB_USER>:<url encoded DB_PASSWORD>@localhost:<DB_PORT>/<DB_NAME>?sslmode=disable"
DB_DSN="host=postgres user=${DB_USER} password=${DB_PASSWORD} dbname=${DB_NAME} port=5432 sslmode=disable TimeZone=UTC"
PRIVATE_KEY="<path to private key>"
PUBLIC_KEY="<path to public key>"
```

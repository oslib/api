package main

import (
	"fmt"
	"github.com/goyek/goyek"
	"os"
	"strings"
)

func migrate(cmd goyek.RegisteredStringParam) goyek.Task {
	return goyek.Task{
		Name:  "migrate",
		Usage: "Migrate the database",
		Action: func(tf *goyek.TF) {
			migrateCmd(tf, cmd)
		},
		Params: goyek.Params{cmd},
	}
}

func migrateCmd(tf *goyek.TF, cmd goyek.RegisteredStringParam) {
	wd, err := os.Getwd()
	if err != nil {
		tf.Fatalf("Could not get wd: %v", err)
	}

	// Prepare command
	args := []string{
		"run",
		"-v",
		fmt.Sprintf("%s/migration:/migrations", wd),
		"--network",
		"host",
		"migrate/migrate",
		"-path=/migrations/",
		"-database",
		os.Getenv("DB_MIGRATE_DSN"),
	}
	args = append(args, strings.Split(cmd.Get(tf), " ")...)

	// Execute command
	if err = tf.Cmd("docker", args...).Run(); err != nil {
		tf.Fatalf("Failed to migrate database: %v", err)
	}
}

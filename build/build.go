package main

import (
	"fmt"
	"github.com/goyek/goyek"
	"gitlab.com/oslib/api/pkg/slice"
	"io/fs"
	"os"
	"strings"
)

func build(service goyek.RegisteredStringParam) goyek.Task {
	return goyek.Task{
		Name:   "build",
		Usage:  "Build the API and services",
		Params: goyek.Params{service},
		Action: func(tf *goyek.TF) {
			buildCmd(tf, service.Get(tf))
		},
	}
}

func buildCmd(tf *goyek.TF, service string) {
	var services []string
	if service != "" {
		services = strings.Split(service, ",")
	}
	if len(services) == 0 || slice.Contains(services, "api") {
		buildApi(tf)
	}
	buildServices(tf, services)
}

func buildApi(tf *goyek.TF) {
	initDir(tf, "./bin")

	wd, err := os.Getwd()
	if err != nil {
		tf.Fatalf("Could not get working directory: %v", err)
	}

	// Run "go build -o 'bin/api' '<wd>/internal/api'
	pkg := fmt.Sprintf("%s/internal/api", wd)
	if err = tf.Cmd("go", "build", "-o", "bin/api", pkg).Run(); err != nil {
		tf.Fatalf("Could not build API: %v", err)
	}
	if err = os.Chown("bin/api", 1000, 1000); err != nil {
		tf.Fatalf("Could not set permissions on output file: %v", err)
	}
}

func buildServices(tf *goyek.TF, services []string) {
	initDir(tf, "./bin")

	wd, err := os.Getwd()
	if err != nil {
		tf.Fatalf("Could not get working directory: %v", err)
	}

	// Build all the services
	dirEntries := getDirEntries(tf, "internal/services")
	for _, dirEntry := range dirEntries {
		if !dirEntry.IsDir() || (len(services) != 0 && !slice.Contains(services, dirEntry.Name())) {
			continue
		}

		// Check if it is a service by seeing if it has a main.go file
		var hasMainFile bool
		subDirEntries := getDirEntries(tf, fmt.Sprintf("internal/services/%s", dirEntry.Name()))
		for _, subDirEntry := range subDirEntries {
			if subDirEntry.Name() == "main.go" {
				hasMainFile = true
			}
		}
		if !hasMainFile {
			continue
		}

		// Build the service
		outputName := fmt.Sprintf("bin/%s_service", dirEntry.Name())
		inputDir := fmt.Sprintf("%s/internal/services/%s", wd, dirEntry.Name())
		if err = tf.Cmd("go", "build", "-o", outputName, inputDir).Run(); err != nil {
			tf.Fatalf("Could not build service %s: %v", dirEntry.Name(), err)
		}
		if err = os.Chown(outputName, 1000, 1000); err != nil {
			tf.Fatalf("Could not set permissions on output file: %v", err)
		}
	}
}

func initDir(tf *goyek.TF, dir string) {
	// Make output directory
	if _, err := os.Stat(dir); err != nil {
		if err = os.Mkdir(dir, fs.ModeDir); err != nil {
			tf.Fatalf("Could not make output directory: %v", err)
		}
		if err = os.Chmod(dir, 0775); err != nil {
			tf.Fatalf("Could not set permissions on output directory: %v", err)
		}
	}
}

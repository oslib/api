package main

import (
	"fmt"
	"github.com/goyek/goyek"
	"os"
	"strings"
)

func grpc(service goyek.RegisteredStringParam) goyek.Task {
	return goyek.Task{
		Name:   "grpc",
		Usage:  "Generate GRPC services",
		Params: goyek.Params{service},
		Action: func(tf *goyek.TF) {
			grpcCmd(tf, service.Get(tf))
		},
	}
}

func grpcCmd(tf *goyek.TF, service string) {
	// Install go packages
	if err := tf.Cmd("go", "install", "google.golang.org/protobuf/cmd/protoc-gen-go@latest").Run(); err != nil {
		tf.Fatalf("Could not install protoc-gen-go: %v", err)
	}
	if err := tf.Cmd("go", "install", "google.golang.org/grpc/cmd/protoc-gen-go-grpc@latest").Run(); err != nil {
		tf.Fatalf("Could not install protoc-gen-go-grpc: %v", err)
	}
	if err := tf.Cmd("go", "install", "github.com/envoyproxy/protoc-gen-validate@v0.6.7").Run(); err != nil {
		tf.Fatalf("Could not install protoc-gen-validate: %v", err)
	}

	// Set path
	if err := tf.Cmd("bash", "-c", "export PATH=$PATH:$(go env GOPATH)/bin").Run(); err != nil {
		tf.Fatalf("Could not set path: %v", err)
	}

	// Get the services for which to generate proto files
	var services []string
	if service != "" {
		service = fmt.Sprintf("internal/proto/%s", service)
		services = append(services, service)
	} else {
		services = getProtoDirs(tf)
	}

	// Generate the proto files for each service
	for _, service = range services {
		protoFiles := getProtoFiles(tf, service)
		if err := tf.Cmd("protoc", getArgs(tf, protoFiles)...).Run(); err != nil {
			tf.Fatal("Failed to generate proto files for %q: %v", service, err)
		}
	}
}

func getProtoDirs(tf *goyek.TF) []string {
	var dirs []string
	dirEntries := getDirEntries(tf, "internal/proto")
	for _, dirEntry := range dirEntries {
		if !dirEntry.IsDir() {
			continue
		}
		dirs = append(dirs, fmt.Sprintf("internal/proto/%s", dirEntry.Name()))
	}
	return dirs
}

func getArgs(tf *goyek.TF, protoFiles []string) []string {
	// Get the GOPATH from the system
	gopath := os.Getenv("GOPATH")
	if gopath == "" {
		tf.Fatalf("GOPATH is not set")
	}

	// Get the args to generate proto files in a certain directory
	args := []string{
		"-I", fmt.Sprintf("%s/pkg/mod", gopath),
		"-I", fmt.Sprintf("%s/pkg/mod/github.com/envoyproxy/protoc-gen-validate@v0.6.7", gopath),
		"--go_out=./internal/proto",
		"--validate_out=lang=go:./internal/proto",
		"--go-grpc_out=./internal/proto",
		"--proto_path=./internal/proto",
	}
	args = append(args, protoFiles...)
	return args
}

func getProtoFiles(tf *goyek.TF, dir string) []string {
	var files []string
	dirEntries := getDirEntries(tf, dir)
	for _, dirEntry := range dirEntries {
		if dirEntry.IsDir() || !strings.HasSuffix(dirEntry.Name(), ".proto") {
			continue
		}
		files = append(files, fmt.Sprintf("%s/%s", dir, dirEntry.Name()))
	}
	return files
}

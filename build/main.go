package main

import (
	"github.com/goyek/goyek"
	"github.com/joho/godotenv"
	"log"
)

func main() {
	if err := godotenv.Load(".env"); err != nil {
		log.Fatalf("Could not load env file: %v\n", err)
	}

	flow := &goyek.Flow{}

	// Set goyek to be default verbose
	flow.RegisterVerboseParam(goyek.BoolParam{
		Name:    "v",
		Usage:   "Verbose: log all tasks as they are run.",
		Default: true,
	})

	// Parameters
	cmd := flow.RegisterStringParam(goyek.StringParam{
		Name:  "cmd",
		Usage: "The migration command to execute",
	})
	service := flow.RegisterStringParam(goyek.StringParam{
		Name:  "service",
		Usage: "The service for which to run the command. If left empty, all services will be used.",
	})

	// Tasks
	flow.Register(build(service))
	flow.Register(buildGoyek())
	flow.Register(grpc(service))
	flow.Register(migrate(cmd))

	flow.Main()
}

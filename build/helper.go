package main

import (
	"github.com/goyek/goyek"
	"os"
)

func getDirEntries(tf *goyek.TF, dir string) []os.DirEntry {
	dirEntries, err := os.ReadDir(dir)
	if err != nil {
		tf.Fatalf("Could not read %q: %v", dir, err)
	}
	return dirEntries
}

package main

import (
	"github.com/goyek/goyek"
)

func buildGoyek() goyek.Task {
	return goyek.Task{
		Name:   "build-goyek",
		Usage:  "Build the Goyek pipelines as binary",
		Action: buildGoyekCmd,
	}
}

func buildGoyekCmd(tf *goyek.TF) {
	if err := tf.Cmd("go", "build", "-o", "./bin/goyek", "./build/...").Run(); err != nil {
		tf.Fatalf("Could not build: %v", err)
	}
}

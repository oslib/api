CREATE TABLE IF NOT EXISTS "book"
(
    "id"           UUID         NOT NULL DEFAULT uuid_generate_v4(),
    "title"        VARCHAR(255) NOT NULL,
    "isbn"         VARCHAR(255) NOT NULL,
    "description"  VARCHAR(2048)         DEFAULT NULL,
    "publisher"    VARCHAR(255)          DEFAULT NULL,
    "image"        VARCHAR(255)          DEFAULT NULL,
    "page_count"   BIGINT                DEFAULT NULL,
    "published_at" BIGINT                DEFAULT NULL, -- Only year
    PRIMARY KEY ("id")
);

CREATE INDEX ON "book" ("isbn");

CREATE TABLE IF NOT EXISTS "book_belongs_to"
(
    "book_id"  UUID NOT NULL,
    "genre_id" UUID NOT NULL,
    PRIMARY KEY ("book_id", "genre_id"),
    FOREIGN KEY ("book_id")
        REFERENCES "book" ("id")
        ON DELETE CASCADE,
    FOREIGN KEY ("genre_id")
        REFERENCES "genre" ("id")
        ON DELETE CASCADE,
    UNIQUE ("book_id", "genre_id")
);

CREATE INDEX ON "book_belongs_to" ("book_id");
CREATE INDEX ON "book_belongs_to" ("genre_id");

CREATE TABLE IF NOT EXISTS "book_written_by"
(
    "book_id"   UUID NOT NULL,
    "author_id" UUID NOT NULL,
    PRIMARY KEY ("book_id", "author_id"),
    FOREIGN KEY ("book_id")
        REFERENCES "book" ("id")
        ON DELETE CASCADE,
    FOREIGN KEY ("author_id")
        REFERENCES "author" ("id")
        ON DELETE CASCADE,
    UNIQUE ("book_id", "author_id")
);

CREATE INDEX ON "book_written_by" ("book_id");
CREATE INDEX ON "book_written_by" ("author_id");

CREATE TABLE IF NOT EXISTS "user_has_book"
(
    "book_id" UUID NOT NULL,
    "user_id" UUID NOT NULL,
    PRIMARY KEY ("book_id", "user_id"),
    FOREIGN KEY ("book_id")
        REFERENCES "book" ("id")
        ON DELETE CASCADE,
    FOREIGN KEY ("user_id")
        REFERENCES "user" ("id")
        ON DELETE CASCADE,
    UNIQUE ("book_id", "user_id")
);

CREATE INDEX ON "user_has_book" ("book_id");
CREATE INDEX ON "user_has_book" ("user_id");

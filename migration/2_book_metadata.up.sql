CREATE TABLE IF NOT EXISTS "author"
(
    "id"   UUID         NOT NULL DEFAULT uuid_generate_v4(),
    "name" VARCHAR(255) NOT NULL UNIQUE,
    PRIMARY KEY ("id")
);

CREATE TABLE IF NOT EXISTS "genre"
(
    "id"   UUID         NOT NULL DEFAULT uuid_generate_v4(),
    "name" VARCHAR(255) NOT NULL UNIQUE,
    PRIMARY KEY ("id")
);

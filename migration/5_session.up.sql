CREATE TABLE IF NOT EXISTS "session"
(
    "id"         UUID NOT NULL            DEFAULT uuid_generate_v4(),
    "user_id"    UUID NOT NULL,
    "created_at" TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
    PRIMARY KEY ("id"),
    FOREIGN KEY ("user_id")
        REFERENCES "user" ("id")
        ON DELETE CASCADE
);

CREATE INDEX ON "session" ("user_id");

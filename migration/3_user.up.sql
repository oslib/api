CREATE TABLE IF NOT EXISTS "user"
(
    "id"       UUID         NOT NULL NOT NULL DEFAULT uuid_generate_v4(),
    "email"    VARCHAR(255) NOT NULL UNIQUE,
    "password" VARCHAR(255) NOT NULL,
    "name"     VARCHAR(255) NOT NULL,
    PRIMARY KEY ("id")
);
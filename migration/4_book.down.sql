DROP TABLE IF EXISTS "user_has_book";
DROP TABLE IF EXISTS "book_belongs_to";
DROP TABLE IF EXISTS "book_written_by";
DROP TABLE IF EXISTS "book";

package user

import (
	"gitlab.com/oslib/api/internal/api/api"
	db "gitlab.com/oslib/api/internal/database"
)

func CreateUser(user *User) error {
	if err := db.GetDB().Create(user).Error; err == nil {
		return nil
	} else if db.GetErrorCode(err) == db.ErrDuplicateKey {
		return api.UserWithEmailExists
	} else {
		return err
	}
}

func GetUser(filter User) (*User, error) {
	var user User
	if err := db.GetDB().Where(filter).First(&user).Error; err != nil {
		return nil, err
	}
	return &user, nil
}

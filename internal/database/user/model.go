package user

import (
	"github.com/google/uuid"
)

type User struct {
	Id       uuid.UUID `gorm:"primaryKey"`
	Name     string
	Email    string
	Password string
}

func (User) TableName() string {
	return "user"
}

package auth

import (
	"github.com/google/uuid"
	"time"
)

type Session struct {
	Id        uuid.UUID `gorm:"primaryKey"`
	UserId    uuid.UUID
	CreatedAt time.Time
}

func (Session) TableName() string {
	return "session"
}

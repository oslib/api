package auth

import (
	db "gitlab.com/oslib/api/internal/database"
)

func CreateSession(session *Session) error {
	return db.GetDB().Create(session).Error
}

func GetSession(filter Session) (*Session, error) {
	var session Session
	if err := db.GetDB().Where(filter).First(&session).Error; err != nil {
		return nil, err
	}
	return &session, nil
}

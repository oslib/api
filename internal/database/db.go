package database

import (
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"log"
	"os"
	"strings"
	"sync"
)

var db *gorm.DB
var once sync.Once

const (
	ErrNullValue    = "23502"
	ErrDuplicateKey = "23505"
)

// GetDB initializes the database connection
func GetDB() *gorm.DB {
	once.Do(func() {
		dbDSN := os.Getenv("DB_DSN")
		var err error
		db, err = gorm.Open(postgres.Open(dbDSN), &gorm.Config{})
		if err != nil {
			log.Fatalf("Failed to initialize db: %s", err)
		}
	})
	return db
}

func GetErrorCode(err error) string {
	if err == nil {
		return ""
	}
	errStr := err.Error()
	s := strings.Index(errStr, "(SQLSTATE ")
	if s == -1 {
		return ""
	}
	newS := errStr[s+len("(SQLSTATE "):]
	e := strings.Index(newS, ")")
	if e == -1 {
		return ""
	}
	return newS[:e]
}

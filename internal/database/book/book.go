package book

import (
	"database/sql"
	"github.com/google/uuid"
	db "gitlab.com/oslib/api/internal/database"
	bpkg "gitlab.com/oslib/api/pkg/books"
	"gorm.io/gorm/clause"
)

func CreateExternalBook(book *bpkg.Book) (*Book, error) {
	var genres []Genre
	for _, genre := range book.Genres {
		genres = append(genres, Genre{
			Id:   uuid.New(),
			Name: genre,
		})
	}
	var authors []Author
	for _, author := range book.Authors {
		authors = append(authors, Author{
			Id:   uuid.New(),
			Name: author,
		})
	}

	tx := db.GetDB().Begin()
	defer tx.Rollback()

	// Create the authors
	if len(authors) > 0 {
		if err := tx.Clauses(clause.OnConflict{
			DoUpdates: clause.AssignmentColumns([]string{"id"}),
			Columns:   []clause.Column{{Name: "name"}},
		}).Create(&authors).Error; err != nil {
			return nil, err
		}
	}
	// Create the genres
	if len(genres) > 0 {
		if err := tx.Clauses(clause.OnConflict{
			DoUpdates: clause.AssignmentColumns([]string{"id"}),
			Columns:   []clause.Column{{Name: "name"}},
		}).Create(&genres).Error; err != nil {
			return nil, err
		}
	}

	toCreate := Book{
		Id:    uuid.New(),
		Title: book.Title,
		Description: sql.NullString{
			Valid:  book.Description != "",
			String: book.Description,
		},
		ISBN: book.ISBN,
		Image: sql.NullString{
			Valid:  book.Image != "",
			String: book.Image,
		},
		Publisher: sql.NullString{
			Valid:  book.Publisher != "",
			String: book.Publisher,
		},
		PageCount: sql.NullInt64{
			Valid: book.PageCount > 0,
			Int64: book.PageCount,
		},
		PublishedAt: sql.NullInt64{
			Valid: book.PublishedAt > 0,
			Int64: book.PublishedAt,
		},
		Genres:  genres,
		Authors: authors,
	}

	if err := tx.Create(&toCreate).Error; err != nil {
		return nil, err
	}
	if err := tx.Commit().Error; err != nil {
		return nil, err
	}

	return &toCreate, nil
}

func GetBook(id uuid.UUID, isbn string) (*Book, error) {
	var book Book
	if err := db.GetDB().
		Preload("Authors").Preload("Genres").
		First(&book, &Book{Id: id, ISBN: isbn}).Error; err != nil {
		return nil, err
	}
	return &book, nil
}

func GetBooks(userId string, limit, offset int64) ([]Book, int64, error) {
	// Fetch the books
	var books []Book
	if err := db.GetDB().
		Preload("Authors").Preload("Genres").
		Offset(int(offset)).Limit(int(limit)).
		Where(`"book".id IN (SELECT book_id FROM user_has_book WHERE user_id = ?)`, userId).
		Find(&books).Error; err != nil {
		return nil, 0, err
	}
	if len(books) == 0 {
		return nil, 0, nil
	}

	// Fetch the total count
	var total int64
	if err := db.GetDB().Model(&Book{}).
		Where(`"book".id IN (SELECT book_id FROM user_has_book WHERE user_id = ?)`, userId).
		Count(&total).Error; err != nil {
		return nil, 0, err
	}

	return books, total, nil
}

func CreateBook(bookId, userId string) error {
	return db.GetDB().Exec(`INSERT INTO user_has_book (user_id, book_id) VALUES (?, ?)`, userId, bookId).Error
}

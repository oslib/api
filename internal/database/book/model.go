package book

import (
	"database/sql"
	"github.com/google/uuid"
	udb "gitlab.com/oslib/api/internal/database/user"
)

type Author struct {
	Id   uuid.UUID `gorm:"primaryKey"`
	Name string    `gorm:"unique"`
	// Relations
	Books []Book `gorm:"many2many:book_written_by"`
}

func (Author) TableName() string {
	return "author"
}

type Genre struct {
	Id   uuid.UUID `gorm:"primaryKey"`
	Name string    `gorm:"unique"`
	// Relations
	Books []Book `gorm:"many2many:book_belongs_to"`
}

func (Genre) TableName() string {
	return "genre"
}

type Book struct {
	Id          uuid.UUID `gorm:"primaryKey"`
	Title       string
	ISBN        string
	Description sql.NullString
	Image       sql.NullString
	Publisher   sql.NullString
	PageCount   sql.NullInt64
	PublishedAt sql.NullInt64
	// Relations
	Genres  []Genre    `gorm:"many2many:book_belongs_to"`
	Authors []Author   `gorm:"many2many:book_written_by"`
	Owners  []udb.User `gorm:"many2many:user_has_book"`
}

func (Book) TableName() string {
	return "book"
}

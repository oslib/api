package main

import (
	"context"
	"github.com/google/uuid"
	"gitlab.com/oslib/api/internal/api/api"
	udb "gitlab.com/oslib/api/internal/database/user"
	userpb "gitlab.com/oslib/api/internal/proto/user"
	"golang.org/x/crypto/bcrypt"
)

type UserService struct {
	userpb.UnimplementedUserServiceServer
}

func newUserService() *UserService {
	return new(UserService)
}

func (u *UserService) CreateUser(_ context.Context, in *userpb.CreateUserRequest) (*userpb.User, error) {
	// Check password length
	if len(in.GetPassword()) < 8 {
		return nil, api.PasswordTooShort
	}

	// Hash password
	hashed, err := bcrypt.GenerateFromPassword([]byte(in.GetPassword()), bcrypt.DefaultCost)
	if err != nil {
		return nil, api.InternalError
	}

	// Transform model
	user := udb.User{
		Id:       uuid.New(),
		Name:     in.GetName(),
		Email:    in.GetEmail(),
		Password: string(hashed),
	}

	// Create user
	if err = udb.CreateUser(&user); err != nil {
		return nil, err
	}
	return userResponse(&user), nil
}

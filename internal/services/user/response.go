package main

import (
	udb "gitlab.com/oslib/api/internal/database/user"
	userpb "gitlab.com/oslib/api/internal/proto/user"
)

func userResponse(user *udb.User) *userpb.User {
	return &userpb.User{
		Id:    user.Id.String(),
		Name:  user.Name,
		Email: user.Email,
	}
}

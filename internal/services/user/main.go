package main

import (
	"gitlab.com/oslib/api/internal/api/api"
	userpb "gitlab.com/oslib/api/internal/proto/user"
)

func main() {
	s := api.SetupService()

	// Register user service
	userService := newUserService()
	userpb.RegisterUserServiceServer(s.Srv, userService)

	s.Serve()
}

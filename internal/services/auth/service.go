package main

import (
	"context"
	"github.com/google/uuid"
	"gitlab.com/oslib/api/internal/api/api"
	adb "gitlab.com/oslib/api/internal/database/auth"
	udb "gitlab.com/oslib/api/internal/database/user"
	"gitlab.com/oslib/api/internal/files"
	authpb "gitlab.com/oslib/api/internal/proto/auth"
	"gitlab.com/oslib/api/pkg/jwt"
	"golang.org/x/crypto/bcrypt"
	"google.golang.org/protobuf/types/known/timestamppb"
	"gorm.io/gorm"
	"time"
)

type AuthService struct {
	authpb.UnimplementedAuthServiceServer
	signer *jwt.Signer
}

func newAuthService() *AuthService {
	return &AuthService{
		signer: jwt.GetSigner(files.PrivateKey, files.PublicKey),
	}
}

func (a *AuthService) CreateSession(_ context.Context, in *authpb.CreateSessionRequest) (*authpb.CreateSessionResponse, error) {
	// Find user by email address
	user, err := udb.GetUser(udb.User{Email: in.GetEmail()})
	if err == gorm.ErrRecordNotFound {
		return nil, api.InvalidCredentials
	} else if err != nil {
		return nil, err
	}

	// Verify password
	if err = bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(in.GetPassword())); err != nil {
		return nil, api.InvalidCredentials
	}

	// Create a session
	session := adb.Session{
		Id:        uuid.New(),
		UserId:    user.Id,
		CreatedAt: time.Now(),
	}
	if err = adb.CreateSession(&session); err != nil {
		return nil, err
	}

	// Create signed token
	expiration := session.CreatedAt.Add(time.Hour * 24 * 30)
	signed, err := a.signer.Sign(session.Id.String(), &expiration)
	if err != nil {
		return nil, err
	}

	return &authpb.CreateSessionResponse{
		UserId:    user.Id.String(),
		Token:     signed,
		CreatedAt: timestamppb.New(session.CreatedAt),
		ExpiresAt: timestamppb.New(expiration),
	}, nil
}

func (a *AuthService) GetSession(_ context.Context, in *authpb.GetSessionRequest) (*authpb.Session, error) {
	// Parse the UUID
	id, err := uuid.Parse(in.GetId())
	if err != nil {
		return nil, api.BadRequest
	}

	// Find the session
	session, err := adb.GetSession(adb.Session{Id: id})
	if err == gorm.ErrRecordNotFound {
		return nil, api.NotFound
	} else if err != nil {
		return nil, err
	}

	// Return the session
	return &authpb.Session{
		UserId:    session.UserId.String(),
		CreatedAt: timestamppb.New(session.CreatedAt),
	}, nil
}

package main

import (
	"gitlab.com/oslib/api/internal/api/api"
	authpb "gitlab.com/oslib/api/internal/proto/auth"
)

func main() {
	s := api.SetupService()

	// Register auth service
	authService := newAuthService()
	authpb.RegisterAuthServiceServer(s.Srv, authService)

	s.Serve()
}

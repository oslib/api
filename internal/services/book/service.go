package main

import (
	"context"
	"github.com/golang/protobuf/ptypes/empty"
	"github.com/google/uuid"
	"gitlab.com/oslib/api/internal/api/api"
	db "gitlab.com/oslib/api/internal/database"
	bdb "gitlab.com/oslib/api/internal/database/book"
	bookpb "gitlab.com/oslib/api/internal/proto/book"
	bookpkg "gitlab.com/oslib/api/pkg/books"
	"gorm.io/gorm"
)

type BookService struct {
	bookpb.UnimplementedBookServiceServer
}

func newBookService() *BookService {
	return new(BookService)
}

func (b *BookService) SearchBook(_ context.Context, in *bookpb.SearchBookRequest) (*bookpb.Book, error) {
	// Check if book is already present in database
	book, err := bdb.GetBook(uuid.Nil, in.GetIsbn())
	if err == nil {
		return bookResponse(book), nil
	} else if err != gorm.ErrRecordNotFound {
		return nil, api.InternalError
	}

	// Try to find the book with an external API
	externalBook, err := bookpkg.GetBook(in.GetIsbn())
	if err == bookpkg.NotFound {
		return nil, api.NotFoundByISBN
	} else if err != nil {
		return nil, err
	}

	// Insert the book into the database
	createdBook, err := bdb.CreateExternalBook(externalBook)
	if err != nil {
		return nil, err
	}
	return bookResponse(createdBook), nil
}

func (b *BookService) GetBooks(_ context.Context, in *bookpb.GetBooksRequest) (*bookpb.Books, error) {
	books, total, err := bdb.GetBooks(in.GetUserId(), in.GetLimit(), in.GetOffset())
	if err != nil {
		return nil, err
	}
	return &bookpb.Books{
		Books: booksResponse(books),
		Total: total,
	}, nil
}

func (b *BookService) CreateBook(_ context.Context, in *bookpb.CreateBookRequest) (*empty.Empty, error) {
	err := bdb.CreateBook(in.GetBookId(), in.GetUserId())
	if db.GetErrorCode(err) == db.ErrDuplicateKey {
		return nil, api.BookAlreadyExists
	} else if err != nil {
		return nil, err
	}
	return new(empty.Empty), nil
}

package main

import (
	"gitlab.com/oslib/api/internal/api/api"
	bookpb "gitlab.com/oslib/api/internal/proto/book"
)

func main() {
	s := api.SetupService()

	// Register book service
	bookService := newBookService()
	bookpb.RegisterBookServiceServer(s.Srv, bookService)

	s.Serve()
}

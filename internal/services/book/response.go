package main

import (
	bdb "gitlab.com/oslib/api/internal/database/book"
	bookpb "gitlab.com/oslib/api/internal/proto/book"
)

func bookResponse(book *bdb.Book) *bookpb.Book {
	authors := make([]*bookpb.Author, 0)
	for _, author := range book.Authors {
		authors = append(authors, &bookpb.Author{
			Id:   author.Id.String(),
			Name: author.Name,
		})
	}
	genres := make([]*bookpb.Genre, 0)
	for _, genre := range book.Genres {
		genres = append(genres, &bookpb.Genre{
			Id:   genre.Id.String(),
			Name: genre.Name,
		})
	}
	return &bookpb.Book{
		Id:          book.Id.String(),
		Title:       book.Title,
		Isbn:        book.ISBN,
		Description: book.Description.String,
		Image:       book.Image.String,
		Publisher:   book.Publisher.String,
		PageCount:   book.PageCount.Int64,
		PublishedAt: book.PublishedAt.Int64,
		Authors:     authors,
		Genres:      genres,
	}
}

func booksResponse(books []bdb.Book) []*bookpb.Book {
	bookResponses := make([]*bookpb.Book, 0, len(books))
	for _, book := range books {
		bookResponses = append(bookResponses, bookResponse(&book))
	}
	return bookResponses
}

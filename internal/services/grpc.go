package services

import (
	validator "github.com/grpc-ecosystem/go-grpc-middleware/validator"
	authpb "gitlab.com/oslib/api/internal/proto/auth"
	bookpb "gitlab.com/oslib/api/internal/proto/book"
	userpb "gitlab.com/oslib/api/internal/proto/user"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"log"
	"sync"
)

var (
	authService authpb.AuthServiceClient
	authOnce    sync.Once

	bookService bookpb.BookServiceClient
	bookOnce    sync.Once

	userService userpb.UserServiceClient
	userOnce    sync.Once
)

func GetAuthService() authpb.AuthServiceClient {
	authOnce.Do(func() {
		conn := getConnection("auth-service:9000")
		authService = authpb.NewAuthServiceClient(conn)
	})
	return authService
}

func GetBookService() bookpb.BookServiceClient {
	bookOnce.Do(func() {
		conn := getConnection("book-service:9000")
		bookService = bookpb.NewBookServiceClient(conn)
	})
	return bookService
}

func GetUserService() userpb.UserServiceClient {
	userOnce.Do(func() {
		conn := getConnection("user-service:9000")
		userService = userpb.NewUserServiceClient(conn)
	})
	return userService
}

// getConnection creates connection to service
func getConnection(address string) *grpc.ClientConn {
	// Connect to service
	conn, err := grpc.Dial(address,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
		grpc.WithUnaryInterceptor(validator.UnaryClientInterceptor()),
	)
	if err != nil {
		log.Fatal(err)
	}
	return conn
}

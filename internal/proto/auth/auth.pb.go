// Code generated by protoc-gen-go. DO NOT EDIT.
// source: auth/auth.proto

package auth

import (
	fmt "fmt"
	_ "github.com/envoyproxy/protoc-gen-validate/validate"
	proto "github.com/golang/protobuf/proto"
	timestamppb "google.golang.org/protobuf/types/known/timestamppb"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

type CreateSessionRequest struct {
	Email                string   `protobuf:"bytes,1,opt,name=email,proto3" json:"email,omitempty"`
	Password             string   `protobuf:"bytes,2,opt,name=password,proto3" json:"password,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *CreateSessionRequest) Reset()         { *m = CreateSessionRequest{} }
func (m *CreateSessionRequest) String() string { return proto.CompactTextString(m) }
func (*CreateSessionRequest) ProtoMessage()    {}
func (*CreateSessionRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_712ec48c1eaf43a2, []int{0}
}

func (m *CreateSessionRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_CreateSessionRequest.Unmarshal(m, b)
}
func (m *CreateSessionRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_CreateSessionRequest.Marshal(b, m, deterministic)
}
func (m *CreateSessionRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_CreateSessionRequest.Merge(m, src)
}
func (m *CreateSessionRequest) XXX_Size() int {
	return xxx_messageInfo_CreateSessionRequest.Size(m)
}
func (m *CreateSessionRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_CreateSessionRequest.DiscardUnknown(m)
}

var xxx_messageInfo_CreateSessionRequest proto.InternalMessageInfo

func (m *CreateSessionRequest) GetEmail() string {
	if m != nil {
		return m.Email
	}
	return ""
}

func (m *CreateSessionRequest) GetPassword() string {
	if m != nil {
		return m.Password
	}
	return ""
}

type CreateSessionResponse struct {
	UserId               string                 `protobuf:"bytes,1,opt,name=user_id,json=userId,proto3" json:"user_id,omitempty"`
	Token                string                 `protobuf:"bytes,2,opt,name=token,proto3" json:"token,omitempty"`
	CreatedAt            *timestamppb.Timestamp `protobuf:"bytes,3,opt,name=created_at,json=createdAt,proto3" json:"created_at,omitempty"`
	ExpiresAt            *timestamppb.Timestamp `protobuf:"bytes,4,opt,name=expires_at,json=expiresAt,proto3" json:"expires_at,omitempty"`
	XXX_NoUnkeyedLiteral struct{}               `json:"-"`
	XXX_unrecognized     []byte                 `json:"-"`
	XXX_sizecache        int32                  `json:"-"`
}

func (m *CreateSessionResponse) Reset()         { *m = CreateSessionResponse{} }
func (m *CreateSessionResponse) String() string { return proto.CompactTextString(m) }
func (*CreateSessionResponse) ProtoMessage()    {}
func (*CreateSessionResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_712ec48c1eaf43a2, []int{1}
}

func (m *CreateSessionResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_CreateSessionResponse.Unmarshal(m, b)
}
func (m *CreateSessionResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_CreateSessionResponse.Marshal(b, m, deterministic)
}
func (m *CreateSessionResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_CreateSessionResponse.Merge(m, src)
}
func (m *CreateSessionResponse) XXX_Size() int {
	return xxx_messageInfo_CreateSessionResponse.Size(m)
}
func (m *CreateSessionResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_CreateSessionResponse.DiscardUnknown(m)
}

var xxx_messageInfo_CreateSessionResponse proto.InternalMessageInfo

func (m *CreateSessionResponse) GetUserId() string {
	if m != nil {
		return m.UserId
	}
	return ""
}

func (m *CreateSessionResponse) GetToken() string {
	if m != nil {
		return m.Token
	}
	return ""
}

func (m *CreateSessionResponse) GetCreatedAt() *timestamppb.Timestamp {
	if m != nil {
		return m.CreatedAt
	}
	return nil
}

func (m *CreateSessionResponse) GetExpiresAt() *timestamppb.Timestamp {
	if m != nil {
		return m.ExpiresAt
	}
	return nil
}

type GetSessionRequest struct {
	Id                   string   `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *GetSessionRequest) Reset()         { *m = GetSessionRequest{} }
func (m *GetSessionRequest) String() string { return proto.CompactTextString(m) }
func (*GetSessionRequest) ProtoMessage()    {}
func (*GetSessionRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_712ec48c1eaf43a2, []int{2}
}

func (m *GetSessionRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_GetSessionRequest.Unmarshal(m, b)
}
func (m *GetSessionRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_GetSessionRequest.Marshal(b, m, deterministic)
}
func (m *GetSessionRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_GetSessionRequest.Merge(m, src)
}
func (m *GetSessionRequest) XXX_Size() int {
	return xxx_messageInfo_GetSessionRequest.Size(m)
}
func (m *GetSessionRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_GetSessionRequest.DiscardUnknown(m)
}

var xxx_messageInfo_GetSessionRequest proto.InternalMessageInfo

func (m *GetSessionRequest) GetId() string {
	if m != nil {
		return m.Id
	}
	return ""
}

type Session struct {
	UserId               string                 `protobuf:"bytes,1,opt,name=user_id,json=userId,proto3" json:"user_id,omitempty"`
	CreatedAt            *timestamppb.Timestamp `protobuf:"bytes,2,opt,name=created_at,json=createdAt,proto3" json:"created_at,omitempty"`
	XXX_NoUnkeyedLiteral struct{}               `json:"-"`
	XXX_unrecognized     []byte                 `json:"-"`
	XXX_sizecache        int32                  `json:"-"`
}

func (m *Session) Reset()         { *m = Session{} }
func (m *Session) String() string { return proto.CompactTextString(m) }
func (*Session) ProtoMessage()    {}
func (*Session) Descriptor() ([]byte, []int) {
	return fileDescriptor_712ec48c1eaf43a2, []int{3}
}

func (m *Session) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_Session.Unmarshal(m, b)
}
func (m *Session) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_Session.Marshal(b, m, deterministic)
}
func (m *Session) XXX_Merge(src proto.Message) {
	xxx_messageInfo_Session.Merge(m, src)
}
func (m *Session) XXX_Size() int {
	return xxx_messageInfo_Session.Size(m)
}
func (m *Session) XXX_DiscardUnknown() {
	xxx_messageInfo_Session.DiscardUnknown(m)
}

var xxx_messageInfo_Session proto.InternalMessageInfo

func (m *Session) GetUserId() string {
	if m != nil {
		return m.UserId
	}
	return ""
}

func (m *Session) GetCreatedAt() *timestamppb.Timestamp {
	if m != nil {
		return m.CreatedAt
	}
	return nil
}

func init() {
	proto.RegisterType((*CreateSessionRequest)(nil), "proto.auth.CreateSessionRequest")
	proto.RegisterType((*CreateSessionResponse)(nil), "proto.auth.CreateSessionResponse")
	proto.RegisterType((*GetSessionRequest)(nil), "proto.auth.GetSessionRequest")
	proto.RegisterType((*Session)(nil), "proto.auth.Session")
}

func init() {
	proto.RegisterFile("auth/auth.proto", fileDescriptor_712ec48c1eaf43a2)
}

var fileDescriptor_712ec48c1eaf43a2 = []byte{
	// 352 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0x94, 0x52, 0xcb, 0x4e, 0xea, 0x40,
	0x18, 0xce, 0x94, 0xfb, 0x4f, 0x4e, 0xce, 0x39, 0x73, 0x8e, 0xb1, 0x69, 0x42, 0x84, 0xba, 0x61,
	0xe3, 0x34, 0xc1, 0x95, 0x3b, 0xa9, 0x0b, 0xe3, 0xb6, 0xb0, 0x62, 0x83, 0x03, 0xfd, 0x85, 0x89,
	0xc0, 0xd4, 0x99, 0x29, 0xfa, 0x42, 0xee, 0x7d, 0x13, 0xdf, 0x87, 0x95, 0xe9, 0x05, 0xb9, 0x48,
	0xbc, 0x6c, 0xda, 0xe6, 0xbb, 0xa5, 0xdf, 0x37, 0x03, 0xbf, 0x79, 0x6c, 0xa6, 0x5e, 0xf2, 0x60,
	0x91, 0x92, 0x46, 0x52, 0x48, 0x5f, 0x2c, 0x41, 0x9c, 0x93, 0x89, 0x94, 0x93, 0x19, 0x7a, 0x29,
	0x34, 0x8a, 0xef, 0x3c, 0x23, 0xe6, 0xa8, 0x0d, 0x9f, 0x47, 0x99, 0xd8, 0x39, 0x5e, 0xf2, 0x99,
	0x08, 0xb9, 0x41, 0x6f, 0xfd, 0x91, 0x11, 0xee, 0x00, 0xfe, 0x5f, 0x29, 0xe4, 0x06, 0x7b, 0xa8,
	0xb5, 0x90, 0x8b, 0x00, 0x1f, 0x62, 0xd4, 0x86, 0x36, 0xa0, 0x84, 0x73, 0x2e, 0x66, 0x36, 0x69,
	0x92, 0x76, 0xcd, 0xaf, 0xac, 0xfc, 0xa2, 0xb2, 0x6e, 0x49, 0x90, 0xa1, 0xf4, 0x14, 0xaa, 0x11,
	0xd7, 0xfa, 0x51, 0xaa, 0xd0, 0xb6, 0xb6, 0x14, 0x7f, 0x48, 0xf0, 0x4e, 0xb8, 0xaf, 0x04, 0x8e,
	0xf6, 0xc2, 0x75, 0x24, 0x17, 0x1a, 0x69, 0x0b, 0x2a, 0xb1, 0x46, 0x35, 0x14, 0x61, 0x9e, 0x5f,
	0x5d, 0xf9, 0x25, 0x55, 0x78, 0x21, 0x24, 0x28, 0x27, 0xc4, 0x4d, 0x98, 0xfc, 0x80, 0x91, 0xf7,
	0xb8, 0xd8, 0x8f, 0xcf, 0x50, 0x7a, 0x01, 0x30, 0x4e, 0xa3, 0xc3, 0x21, 0x37, 0x76, 0xa1, 0x49,
	0xda, 0xf5, 0x8e, 0xc3, 0xb2, 0x19, 0xd8, 0x7a, 0x06, 0xd6, 0x5f, 0xcf, 0x10, 0xd4, 0x72, 0x75,
	0xd7, 0x24, 0x56, 0x7c, 0x8a, 0x84, 0x42, 0x9d, 0x58, 0x8b, 0x5f, 0x5b, 0x73, 0x75, 0xd7, 0xb8,
	0x67, 0xf0, 0xf7, 0x1a, 0xcd, 0xde, 0x54, 0x36, 0x58, 0x07, 0x7a, 0x58, 0x22, 0x74, 0x27, 0x50,
	0xc9, 0xb5, 0xdf, 0x69, 0xbc, 0x5b, 0xc9, 0xfa, 0x41, 0xa5, 0xce, 0x33, 0x81, 0x7a, 0x37, 0x36,
	0xd3, 0x1e, 0xaa, 0xa5, 0x18, 0x23, 0xed, 0xc3, 0xaf, 0x9d, 0xe1, 0x69, 0x93, 0x6d, 0x6e, 0x0b,
	0x3b, 0x74, 0xe0, 0x4e, 0xeb, 0x13, 0x45, 0x7e, 0x6a, 0x97, 0x00, 0x9b, 0xf6, 0xb4, 0xb1, 0x6d,
	0xf8, 0xb0, 0x8a, 0xf3, 0x6f, 0x9b, 0xce, 0x39, 0xbf, 0x3a, 0x28, 0xb3, 0xf4, 0x0e, 0x8f, 0xca,
	0x29, 0x7b, 0xfe, 0x16, 0x00, 0x00, 0xff, 0xff, 0x41, 0xf7, 0xd6, 0x7e, 0xd7, 0x02, 0x00, 0x00,
}

package api

import (
	"github.com/gin-gonic/gin"
	authpb "gitlab.com/oslib/api/internal/proto/auth"
)

const (
	// Session contains the auth session of this request
	Session = "session"
)

func GetSession(c *gin.Context) *authpb.Session {
	return c.MustGet(Session).(*authpb.Session)
}

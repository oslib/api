package api

import (
	"github.com/gin-gonic/gin"
	"log"
	"net/http"
	"os"
	"time"
)

type Option string

// SetupAPI configures Gin with some default settings
func SetupAPI(options ...Option) *gin.Engine {
	r := gin.New()

	if os.Getenv("MODE") == "release" {
		gin.SetMode(gin.ReleaseMode)
	}

	// Set max 8MB for multipart forms (default is 32MB)
	r.MaxMultipartMemory = 8 << 20
	// Register gin middleware
	r.Use(gin.Logger(),
		gin.CustomRecovery(func(c *gin.Context, err interface{}) {
			log.Printf("Recovering: %v", err)
			Error(c, InternalError)
		}),
	)
	return r
}

// GetRouterGroup to register routes on for the API.
func GetRouterGroup(r *gin.Engine, handlers ...gin.HandlerFunc) *gin.RouterGroup {
	return r.Group("/v1", handlers...)
}

// ServeAPI starts a Gin engine
func ServeAPI(r *gin.Engine) {
	server := &http.Server{
		Addr:           "0.0.0.0:8080",
		Handler:        r,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}
	if err := server.ListenAndServe(); err != nil {
		log.Panicf("Failed to serve API: %s", err)
	}
}

package api

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/oslib/api/internal/files"
	authpb "gitlab.com/oslib/api/internal/proto/auth"
	grpcs "gitlab.com/oslib/api/internal/services"
	"gitlab.com/oslib/api/pkg/jwt"
	"strings"
)

// AuthMiddleware checks if the user is authenticated
func AuthMiddleware() gin.HandlerFunc {
	signer := jwt.GetSigner(files.PrivateKey, files.PublicKey)

	return func(c *gin.Context) {
		// Check if token is present
		value := c.GetHeader("Authorization")
		if value == "" || !strings.HasPrefix(value, "Bearer ") {
			Error(c, Unauthorized)
			return
		}

		// Check if token is valid
		token := strings.TrimPrefix(value, "Bearer ")
		claims, err := signer.Verify(token)
		if err != nil {
			Error(c, Unauthorized)
			return
		}

		// Get the corresponding session
		as := grpcs.GetAuthService()
		session, err := as.GetSession(c, &authpb.GetSessionRequest{Id: claims.ID})
		if err == NotFound {
			Error(c, Unauthorized)
			return
		} else if err != nil {
			Error(c, InternalError)
			return
		}

		// Store session in context
		c.Set(Session, session)
		c.Next()
	}
}

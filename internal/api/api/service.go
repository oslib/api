package api

import (
	middleware "github.com/grpc-ecosystem/go-grpc-middleware"
	recovery "github.com/grpc-ecosystem/go-grpc-middleware/recovery"
	validator "github.com/grpc-ecosystem/go-grpc-middleware/validator"
	"google.golang.org/grpc"
	"log"
	"net"
)

type Service struct {
	Srv *grpc.Server
	Lis net.Listener
}

// SetupService creates a socket + server for a service.
func SetupService() *Service {
	// Socket used by grpc server
	lis, err := net.Listen("tcp", ":9000")
	if err != nil {
		log.Fatalf("Failed to listen on socket: %v", err)
	}

	server := grpc.NewServer(
		grpc.UnaryInterceptor(middleware.ChainUnaryServer(
			validator.UnaryServerInterceptor(),
			recovery.UnaryServerInterceptor(
				recovery.WithRecoveryHandler(func(p interface{}) error {
					log.Println(p)
					return InternalError
				}),
			),
		)),
	)

	return &Service{
		Srv: server,
		Lis: lis,
	}
}

// Serve serves service as GRPC server.
func (s *Service) Serve() {
	log.Print("Serve grpc service")
	if err := s.Srv.Serve(s.Lis); err != nil {
		log.Fatalf("Failed to serve %s", err)
	}
}

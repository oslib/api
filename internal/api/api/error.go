package api

import (
	"github.com/gin-gonic/gin"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"log"
	"net/http"
)

type ErrorCode string

func (e ErrorCode) Error() string {
	return string(e)
}

type Details struct {
	HttpCode    int
	Description string
}

const (
	BookAlreadyExists   ErrorCode = "BookAlreadyExists"
	InternalError       ErrorCode = "InternalError"
	BadRequest          ErrorCode = "BadRequest"
	NotFoundByISBN      ErrorCode = "NotFoundByISBN"
	UserWithEmailExists ErrorCode = "UserWithEmailExists"
	PasswordTooShort    ErrorCode = "PasswordTooShort"
	InvalidCredentials  ErrorCode = "InvalidCredentials"
	Unauthorized        ErrorCode = "Unauthorized"
	NotFound            ErrorCode = "NotFound"
)

var errors = map[ErrorCode]Details{
	BookAlreadyExists:   {http.StatusConflict, "Book already exists"},
	InternalError:       {http.StatusInternalServerError, "Internal server error"},
	BadRequest:          {http.StatusBadRequest, "Bad request"},
	NotFoundByISBN:      {http.StatusNotFound, "The book with the specified ISBN could not be found"},
	UserWithEmailExists: {http.StatusConflict, "A user with this email-address already exists"},
	PasswordTooShort:    {http.StatusBadRequest, "Password has to be at least 8 characters"},
	InvalidCredentials:  {http.StatusUnauthorized, "User with the specified credentials is not found"},
	Unauthorized:        {http.StatusUnauthorized, "There is no valid token present in the request"},
	NotFound:            {http.StatusNotFound, "The requested resource could not be found"},
}

// Error writes an error as response
func Error(c *gin.Context, err error) {
	log.Printf("Sending error: %v", err)
	var httpCode int
	var message gin.H
	var errStr string

	// Parse error if it is a GRPC error
	if e, ok := status.FromError(err); ok {
		if e.Code() == codes.InvalidArgument {
			errStr = BadRequest.Error()
		} else {
			errStr = e.Message()
		}
	} else {
		errStr = err.Error()
	}

	if info, exists := errors[ErrorCode(errStr)]; exists {
		httpCode, message = info.HttpCode, gin.H{
			"success":    false,
			"error":      info.Description,
			"error_code": ErrorCode(errStr),
		}
	} else {
		httpCode, message = http.StatusInternalServerError, gin.H{
			"success":    false,
			"error":      "Internal error",
			"error_code": InternalError,
		}
	}

	c.AbortWithStatusJSON(httpCode, message)
}

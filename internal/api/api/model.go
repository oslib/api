package api

type PageRequest struct {
	Page  int64 `form:"page" json:"page" binding:"required"`
	Limit int64 `form:"limit" json:"pageSize" binding:"required"`
}

func (p PageRequest) GetOffset() int64 {
	return (p.Page - 1) * p.Limit
}

package api

import (
	"github.com/gin-gonic/gin"
	"math"
	"net/http"
	"reflect"
)

type SuccessResponse struct {
	Success bool `json:"success"`
}

func Get(c *gin.Context, data interface{}) {
	if data == nil {
		c.JSON(http.StatusNoContent, nil)
	} else {
		c.JSON(http.StatusOK, map[string]interface{}{
			"success": true,
			"data":    data,
		})
	}
}

func List(c *gin.Context, page, limit, total int64, data interface{}) {
	// Get total number of pages
	var pages int64 = 1
	if total > 0 {
		pages = int64(math.Ceil(float64(total) / float64(limit)))
	}

	// Get number of items in array
	count := 0
	if kind := reflect.TypeOf(data).Kind(); kind == reflect.Slice || kind == reflect.Array {
		count = reflect.ValueOf(data).Len()
	}

	if data == nil {
		data = make([]interface{}, 0)
	}
	c.JSON(http.StatusOK, map[string]interface{}{
		"success": true,
		"page":    page,
		"pages":   pages,
		"limit":   limit,
		"total":   total,
		"count":   count,
		"data":    data,
	})
}

func Created(c *gin.Context, data interface{}) {
	if data == nil {
		c.JSON(http.StatusNoContent, nil)
	} else {
		c.JSON(http.StatusCreated, map[string]interface{}{
			"success": true,
			"data":    data,
		})
	}
}

package main

import (
	"gitlab.com/oslib/api/internal/api/api"
	"gitlab.com/oslib/api/internal/api/auth"
	"gitlab.com/oslib/api/internal/api/book"
	"gitlab.com/oslib/api/internal/api/user"
)

func main() {
	// Setup API
	e := api.SetupAPI()

	// Setup router with auth
	v1NoAuth := api.GetRouterGroup(e)
	v1 := api.GetRouterGroup(e, api.AuthMiddleware())

	{
		// Books
		v1.GET("/book/search", book.SearchBook)
		v1.GET("/book", book.GetBooks)
		v1.POST("/book", book.CreateBook)

		// Sessions
		v1NoAuth.POST("/auth/session", auth.CreateSession)

		// Users
		v1NoAuth.POST("/user", user.CreateUser)
	}

	// Serve API
	api.ServeAPI(e)
}

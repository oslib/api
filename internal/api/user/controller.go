package user

import (
	"github.com/gin-gonic/gin"
	api "gitlab.com/oslib/api/internal/api/api"
	userpb "gitlab.com/oslib/api/internal/proto/user"
	grpcs "gitlab.com/oslib/api/internal/services"
)

// CreateUser Create a user with unique email address.
func CreateUser(c *gin.Context) {
	// Verify request
	var body CreateUserRequest
	if err := c.ShouldBindJSON(&body); err != nil {
		api.Error(c, api.BadRequest)
		return
	}

	// Create user with service
	us := grpcs.GetUserService()
	res, err := us.CreateUser(c, &userpb.CreateUserRequest{
		Name:     body.Name,
		Email:    body.Email,
		Password: body.Password,
	})
	if err != nil {
		api.Error(c, err)
		return
	}

	// Send response
	api.Created(c, userResponse(res))
}

package user

import (
	userpb "gitlab.com/oslib/api/internal/proto/user"
)

func userResponse(user *userpb.User) User {
	return User{
		Id:    user.GetId(),
		Email: user.GetEmail(),
		Name:  user.GetName(),
	}
}

package auth

import (
	authpb "gitlab.com/oslib/api/internal/proto/auth"
)

func sessionResponse(pb *authpb.CreateSessionResponse) Session {
	return Session{
		UserId:    pb.GetUserId(),
		Token:     pb.GetToken(),
		CreatedAt: pb.GetCreatedAt().AsTime(),
		ExpiresAt: pb.GetExpiresAt().AsTime(),
	}
}

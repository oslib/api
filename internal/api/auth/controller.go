package auth

import (
	"github.com/gin-gonic/gin"
	api "gitlab.com/oslib/api/internal/api/api"
	authpb "gitlab.com/oslib/api/internal/proto/auth"
	grpcs "gitlab.com/oslib/api/internal/services"
)

// CreateSession Create a session for a user.
func CreateSession(c *gin.Context) {
	// Verify request
	var body CreateSessionRequest
	if err := c.ShouldBindJSON(&body); err != nil {
		api.Error(c, api.BadRequest)
		return
	}

	authService := grpcs.GetAuthService()
	res, err := authService.CreateSession(c, &authpb.CreateSessionRequest{
		Email:    body.Email,
		Password: body.Password,
	})
	if err != nil {
		api.Error(c, err)
		return
	}

	// Return response
	api.Created(c, sessionResponse(res))
}

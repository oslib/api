package book

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/oslib/api/internal/api/api"
	bookpb "gitlab.com/oslib/api/internal/proto/book"
	grpcs "gitlab.com/oslib/api/internal/services"
)

// SearchBook Search for a book by ISBN. First tries to search in own
// database, if not found, tries to search in external API.
func SearchBook(c *gin.Context) {
	// Verify request
	var uri SearchBookQuery
	if err := c.ShouldBindQuery(&uri); err != nil {
		api.Error(c, api.BadRequest)
		return
	}

	// Fetch book from service
	bs := grpcs.GetBookService()
	res, err := bs.SearchBook(c, &bookpb.SearchBookRequest{
		Isbn: uri.ISBN,
	})
	if err != nil {
		api.Error(c, err)
		return
	}

	// Return response
	api.Get(c, bookResponse(res))
}

// GetBooks fetches a list of books of the logged-in user
func GetBooks(c *gin.Context) {
	// Validate request
	var query api.PageRequest
	if err := c.ShouldBindQuery(&query); err != nil {
		api.Error(c, api.BadRequest)
		return
	}

	// Get the user for whom to fetch the books
	user := api.GetSession(c).UserId

	// Fetch books from service
	bs := grpcs.GetBookService()
	res, err := bs.GetBooks(c, &bookpb.GetBooksRequest{
		UserId: user,
		Offset: query.GetOffset(),
		Limit:  query.Limit,
	})
	if err != nil {
		api.Error(c, err)
		return
	}

	// Return response
	books := booksResponse(res.GetBooks())
	api.List(c, query.Page, query.Limit, res.GetTotal(), books)
}

// CreateBook connects a book to a user
func CreateBook(c *gin.Context) {
	// Validate the request
	var body CreateBookRequest
	if err := c.ShouldBindJSON(&body); err != nil {
		api.Error(c, api.BadRequest)
		return
	}

	// Get the user for whom to create the book
	user := api.GetSession(c).UserId

	// Create book in service
	bs := grpcs.GetBookService()
	_, err := bs.CreateBook(c, &bookpb.CreateBookRequest{
		UserId: user,
		BookId: body.BookId,
	})
	if err != nil {
		api.Error(c, err)
		return
	}

	// Return response
	api.Created(c, nil)
}

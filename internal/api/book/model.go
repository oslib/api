package book

type SearchBookQuery struct {
	ISBN string `form:"isbn" binding:"required"`
}

type Author struct {
	Id   string `json:"id"`
	Name string `json:"name"`
}

type Genre struct {
	Id   string `json:"id"`
	Name string `json:"name"`
}

type Book struct {
	ID          string   `json:"id"`
	Title       string   `json:"title"`
	ISBN        string   `json:"isbn"`
	Description string   `json:"description,omitempty"`
	Image       string   `json:"image,omitempty"`
	Publisher   string   `json:"publisher,omitempty"`
	PageCount   int64    `json:"pageCount,omitempty"`
	PublishedAt int64    `json:"publishedAt,omitempty"`
	Authors     []Author `json:"authors"`
	Genres      []Genre  `json:"genres"`
}

type CreateBookRequest struct {
	BookId string `json:"bookId" binding:"required,uuid"`
}

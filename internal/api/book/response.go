package book

import (
	bookpb "gitlab.com/oslib/api/internal/proto/book"
)

func bookResponse(b *bookpb.Book) Book {
	authors := make([]Author, 0)
	for _, author := range b.Authors {
		authors = append(authors, Author{
			Id:   author.GetId(),
			Name: author.GetName(),
		})
	}
	genres := make([]Genre, 0)
	for _, genre := range b.Genres {
		genres = append(genres, Genre{
			Id:   genre.GetId(),
			Name: genre.GetName(),
		})
	}
	return Book{
		ID:          b.GetId(),
		Title:       b.GetTitle(),
		ISBN:        b.GetIsbn(),
		Description: b.GetDescription(),
		Image:       b.GetImage(),
		Publisher:   b.GetPublisher(),
		PageCount:   b.GetPageCount(),
		PublishedAt: b.GetPublishedAt(),
		Authors:     authors,
		Genres:      genres,
	}
}

func booksResponse(books []*bookpb.Book) []Book {
	response := make([]Book, 0, len(books))
	for _, book := range books {
		response = append(response, bookResponse(book))
	}
	return response
}
